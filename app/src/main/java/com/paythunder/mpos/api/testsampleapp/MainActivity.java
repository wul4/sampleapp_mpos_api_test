package com.paythunder.mpos.api.testsampleapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;

import com.paythunder.mpos.api.PayThunderPaymentActivity;
import com.paythunder.mpos.api.access.PayThunderAccess;
import com.paythunder.mpos.api.interfaces.IPayThunderInit;
import com.paythunder.mpos.api.models.PayThunderCredential;
import com.paythunder.mpos.api.utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements IPayThunderInit {

    public static final int PAYTHUNDER_REQUEST = 11;
    public static final String TAG = MainActivity.class.getName();

    private ImageView iv_connection_status;
    private EditText amount;
    private EditText currency;
    private EditText concept;
    private EditText phone;
    private EditText email;
    private EditText merchantRequestID;
    private Button btn_payment_request;
    private Switch sw_response;
    private EditText usercom;

    private Context context;

    final String DATA_CODAUTORIZACION = "coda";
    final String DATA_IMPORTE = "imp";
    final String DATA_FECHA = "fec";
    final String DATA_TIPOMEDIOPAGO = "tiop";
    final String DATA_TELEFONO = "tf";
    final String DATA_RESPUESTA = "resp";
    final String DATA_IDOPERACION = "ido";
    final String DATA_CONFIRMARPAGO = "conf";
    final String DATA_MONEDA = "mon";
    final String DATA_PETICION = "pet";
    final String DATA_MERCHANTREQUESTID = "merchantRequestID";
    final int RESULT_PAYMENT_DEFERRED = 2;

    // /////////////////////////////////////////////////////////////////////////////////////
    //  ACTIVITY LIFECYCLE
    // /////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        amount = (EditText) findViewById(R.id.amount);
        currency = (EditText) findViewById(R.id.currency);
        concept = (EditText) findViewById(R.id.concept);
        phone = (EditText) findViewById(R.id.phone);
        email = (EditText) findViewById(R.id.email);
        merchantRequestID = (EditText) findViewById(R.id.merchant_op_id);

        iv_connection_status = (ImageView) findViewById(R.id.iv_pt_connection);
        btn_payment_request = (Button) findViewById(R.id.button);
        sw_response = (Switch) findViewById(R.id.sw_response);
        usercom = (EditText) findViewById(R.id.usercom);

        amount.requestFocus();
        context = this;

        // Init PayThunder
        initPayThunder();
    }

    // MANAGE PAYMENT RESPONSE
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        String error;
        int error_code;

        String merchantRequestID = "";

        if(data != null)
            if(data.getStringExtra(DATA_MERCHANTREQUESTID) != null)
                merchantRequestID = data.getStringExtra(DATA_MERCHANTREQUESTID);


        if (requestCode == PAYTHUNDER_REQUEST) {
            switch (resultCode) {
                case RESULT_OK:
                    showSuccessDialog(data);
                    break;

                case RESULT_FIRST_USER:
                    if (data != null) {
                        error = data.getStringExtra(PayThunderPaymentActivity.EXTRA_PAYMENT_RESULT);
                        error_code = data.getIntExtra(PayThunderPaymentActivity.EXTRA_PAYMENT_RESULT_CODE, -1);

                        switch (error_code){

                            case PayThunderPaymentActivity.ERROR_CODE_NO_MPOS:
                                // PayThunder mPOS App not installed
                                // do something
                                break;

                            case PayThunderPaymentActivity.ERROR_CODE_NO_INTERNET_CONNECTION:
                                // No Internet connection available
                                // do something
                                break;

                            case PayThunderPaymentActivity.ERROR_CODE_PAYTHUNDER_NO_INITIATED:
                                // PayThunder mPOS API has not been initialized
                                // do something
                                break;

                            case PayThunderPaymentActivity.ERROR_CODE_INVALID_SESSION:
                                // The session is expired. You must login.
                                // do something
                                break;

                            case PayThunderPaymentActivity.ERROR_CODE_INVALID_CREDENTIALS:
                                // The credentials provided are invalid
                                // do something
                                break;

                            case PayThunderPaymentActivity.ERROR_CODE_REQUIRED_PARAM_MISSING:
                                // A required parameter is missing
                                // do something
                                break;

                            default:
                                // Unexpected error
                                // do something
                                break;
                        }

                        // show error description
                        showErrorDialog(error, merchantRequestID);

                    }
                    break;

                case RESULT_CANCELED:

                    error = getResources().getString(R.string.operation_canceled);

                    showErrorDialog(error, merchantRequestID);
                    break;


                case RESULT_PAYMENT_DEFERRED:

                    showPaymentDeferred(merchantRequestID);
                    break;


                default:

                    error = getResources().getString(R.string.error_returning_data);

                    showErrorDialog(error, merchantRequestID);

                    break;
            }
        }
    }
    // /////////////////////////////////////////////////////////////////////////////////////

    // /////////////////////////////////////////////////////////////////////////////////////
    //  REQUESTS
    // /////////////////////////////////////////////////////////////////////////////////////
    // INIT PAYTHUNDER
    public void initPayThunder(){
        PayThunderAccess.init(this);
    }

    // DO PAYMENT REQUEST
    public void requestPayThunderMPOS(View v) {

        Intent payThunderIntent = new Intent(this, PayThunderPaymentActivity.class);
        Bundle bundle = new Bundle();

        if (amount.getText().toString().isEmpty()) {
            // bundle.putString(Constants.REQUEST_AMOUNT,Constants.DEFAULT_AMOUNT);
        } else {bundle.putString(Constants.REQUEST_AMOUNT, amount.getText().toString());}

        if (currency.getText().toString().isEmpty()) {
            bundle.putString(Constants.REQUEST_CURRENCY, Constants.DEFAULT_CURRENCY);
        } else {bundle.putString(Constants.REQUEST_CURRENCY, currency.getText().toString());}

        if (concept.getText().toString().isEmpty()) {
            bundle.putString(Constants.REQUEST_CONCEPT, Constants.DEFAULT_CONCEPT);
        } else {bundle.putString(Constants.REQUEST_CONCEPT, concept.getText().toString());}


        if (phone.getText().toString().isEmpty()) {
            // bundle.putString(Constants.REQUEST_PHONE, Constants.DEFAULT_PHONE);
        } else {bundle.putString(Constants.REQUEST_PHONE, phone.getText().toString());}


        if (email.getText().toString().isEmpty()) {
        } else {bundle.putString(Constants.REQUEST_EMAIL, email.getText().toString());}

        if (merchantRequestID.getText().toString().isEmpty()) {
        } else {bundle.putString(Constants.REQUEST_MERCHANT_ID, merchantRequestID.getText().toString());}

        if (usercom.getText().toString().isEmpty()) {
        } else {bundle.putString(Constants.REQUEST_USER_COM, usercom.getText().toString());}

        bundle.putString(Constants.REQUEST_PHONE_CC, Constants.DEFAULT_PHONE_CC);

        bundle.putBoolean(Constants.REQUEST_BACKGROUND, !sw_response.isChecked());
        
        payThunderIntent.putExtras(bundle);
        startActivityForResult(payThunderIntent, PAYTHUNDER_REQUEST);
    }
    // /////////////////////////////////////////////////////////////////////////////////////

    // /////////////////////////////////////////////////////////////////////////////////////
    //  RESPONSES
    // /////////////////////////////////////////////////////////////////////////////////////
    // INITIALIZATION RESULTS
    @Override
    public void initSuccess() {
        iv_connection_status.setImageResource(R.drawable.connected);
        btn_payment_request.setEnabled(true);
    }

    @Override
    public void initError(String error) {
        iv_connection_status.setImageResource(R.drawable.disconnected);
        btn_payment_request.setEnabled(false);
    }
    // /////////////////////////////////////////////////////////////////////////////////////

    // /////////////////////////////////////////////////////////////////////////////////////
    //  AUX
    // /////////////////////////////////////////////////////////////////////////////////////
    private void showErrorDialog(String error, String merchantRequestID) {

        String title = getResources().getString(R.string.error_payment_requesting_title);

        if(merchantRequestID != null)
            if(!merchantRequestID.trim().isEmpty())
                title += ": " + merchantRequestID;


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(error)
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        // do nothing

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    private void showPaymentDeferred(String merchantRequestID) {

        String title = getResources().getString(R.string.deferred_payment_requesting_title);

        if(merchantRequestID != null)
            if(!merchantRequestID.trim().isEmpty())
                title += ": " + merchantRequestID;

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.deferred_payment_requesting_message))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        // do nothing

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    private void showSuccessDialog(Intent data) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        String message = "";

        if(data != null) {

            // set title
            alertDialogBuilder.setTitle(getResources().getString(R.string.payment_ok_title));


            Bundle extras = data.getExtras();

            if (extras != null) {

                try {

                    String codAutorizacion = extras.getString(DATA_CODAUTORIZACION);
                    String respuesta = extras.getString(DATA_RESPUESTA);
                    String telefono = extras.getString(DATA_TELEFONO);
                    String importe = extras.getString(DATA_IMPORTE);
                    String tipoMedioPago = extras.getString(DATA_TIPOMEDIOPAGO);
                    String fecha = extras.getString(DATA_FECHA);
                    String moneda = extras.getString(DATA_MONEDA);
                    String peticion = extras.getString(DATA_PETICION);
                    String confirmarPago = extras.getString(DATA_CONFIRMARPAGO);
                    String operacionId = extras.getString(DATA_IDOPERACION);
                    String merchantRequestID = extras.getString(DATA_MERCHANTREQUESTID);

                    message += getResources().getString(R.string.operationid) + operacionId + "\n";
                    message += getResources().getString(R.string.date) + formatDate(fecha) + "\n";
                    message += getResources().getString(R.string.telephone)  + telefono + "\n";
                    message += getResources().getString(R.string.amount) + importe + "\n";
                    message += getResources().getString(R.string.currency) + moneda + "\n";
                    message += getResources().getString(R.string.cod_auth) + codAutorizacion + "\n";
                    message += getResources().getString(R.string.merchantRequestID) + merchantRequestID + "\n";


                } catch (NullPointerException exception) {
                }
            }

        } else{

            String title = getResources().getString(R.string.payment_request_ok_title);

            if(merchantRequestID!=null)
                if(!merchantRequestID.getText().toString().isEmpty())
                    title += ": " + merchantRequestID.getText().toString();


            // set title
            alertDialogBuilder.setTitle(title);

            message += getResources().getString(R.string.payment_request_sent_to);
            message += " <b>";
            message += email.getText();
            message += "</b> ";
            message += getResources().getString(R.string.successfully);


        }

        // set dialog message
        alertDialogBuilder
                .setMessage(Html.fromHtml(message))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        // do nothing

                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public String formatDate(String date_str) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);

        Date date = new Date();
        try {
            date = sdf.parse(date_str);
        } catch (ParseException e) {
            Log.wtf(TAG, e.getMessage());
        }
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
        String resultado = formato.format(date);
        return resultado;
    }
    // /////////////////////////////////////////////////////////////////////////////////////

}